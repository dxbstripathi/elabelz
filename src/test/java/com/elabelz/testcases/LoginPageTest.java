package com.elabelz.testcases;



import static org.testng.Assert.assertTrue;


import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;


import com.elabelz.base.TestBase;
import com.elabelz.pages.CheckoutPage;
import com.elabelz.pages.HomePage;

import com.elabelz.pages.LoginPage;
import com.elabelz.util.TestUtil;


public class LoginPageTest extends TestBase {
	
	HomePage homePage;
	TestUtil testUtil;
	LoginPage loginPage;
	CheckoutPage checkoutPage;
	
	public LoginPageTest(){
		super();
	}
	
	@BeforeMethod
	public void setUp(){
		initialization();
		testUtil = new TestUtil();
		homePage = new HomePage();
		loginPage = homePage.clickOnSignIn();
	}
	
	
	  @Test(priority=1) 
	  public void loginPageTest(){ 
		  
	  checkoutPage = loginPage.login(prop.getProperty("username"), prop.getProperty("password"));
	  String title =checkoutPage.validateLoginPageTitle(); 
	  assertTrue(title.contains("Welcome to your account."));
	  
	  }
	  
	  
	 
	
	
	
	
	
	@AfterMethod
	public void tearDown(){
		driver.quit();
	}

}
