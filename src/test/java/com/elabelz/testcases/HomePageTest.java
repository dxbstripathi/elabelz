package com.elabelz.testcases;

import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;


import com.elabelz.base.TestBase;
import com.elabelz.pages.HomePage;
import com.elabelz.pages.LoginPage;
import com.elabelz.util.TestUtil;

public class HomePageTest extends TestBase {
	
	

	HomePage homePage;
	TestUtil testUtil;
	LoginPage loginPage;
	//HomePage homePage;
	//WebDriverWait wait;
	
	
	public HomePageTest() {
		super();
	}

	
	
	@BeforeMethod
	public void setUp() {
		initialization();
		testUtil = new TestUtil();
		homePage = new HomePage();
		
	
	}
	
	
	
	@Test(priority=1)
	public void loginTest(){
		loginPage = homePage.clickOnSignIn();
		String homePageTitle = homePage.verifyHomePageTitle();
		Assert.assertEquals(homePageTitle, "Login - My Store","Home page title not matched");
	}
	

	
	
	
	@AfterMethod
	public void tearDown(){
		driver.quit();
	}
	

}
