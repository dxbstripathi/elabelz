package com.elabelz.testcases;


import java.util.Date;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.elabelz.base.TestBase;
import com.elabelz.pages.HomePage;
import com.elabelz.pages.LandingPage;
import com.elabelz.pages.LoginPage;
import com.elabelz.pages.RegistrationPage;
import com.elabelz.util.TestUtil;

public class RegistrationPageTest extends TestBase{
	
	
	HomePage homePage;
	TestUtil testUtil;
	LoginPage loginPage;
	LandingPage landingPage;
	RegistrationPage registrationPage;
	 String sheetName = "createAcct";
	
	public RegistrationPageTest(){
		super();
	}
	
	@BeforeMethod
	public void setUp(){
		initialization();
		testUtil = new TestUtil();
		homePage = new HomePage();
		loginPage = homePage.clickOnSignIn();
		registrationPage = new RegistrationPage();
		String timestamp = String.valueOf(new Date().getTime());
	     
		
		

	}
	
	@DataProvider
	public Object [][] getAcctCreateData(){
		Object [][] data=TestUtil.getTestData(sheetName);
		return data;
		
	}
	
	

		
		@Test(priority=1, dataProvider="getAcctCreateData")
		public void createAccountTest(String email, String custFName, String custLName, String password, String days, String months, String years, String fName, String lName, String comp, String adds1, String adds2, String cityOfAdd, String cState, String pCode, String country, String landphone, String pMobile, String aliasEmail) {
			registrationPage.createAccount(email, custFName, custLName, password, days, months, years, fName, lName, comp, adds1, adds2, cityOfAdd, cState, pCode, country, landphone, pMobile, aliasEmail);
			
			String registrationPageText  = registrationPage.validatePageTitle();
			Assert.assertEquals(registrationPageText, "MY ACCOUNT","My Account Text not matched");
			
		}
		
		
		
	
		
		
		@AfterMethod
		public void tearDown(){
			driver.quit();
		}

}
