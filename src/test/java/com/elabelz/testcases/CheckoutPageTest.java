package com.elabelz.testcases;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.elabelz.base.TestBase;
import com.elabelz.pages.CheckoutPage;
import com.elabelz.pages.HomePage;
import com.elabelz.pages.LandingPage;
import com.elabelz.pages.LoginPage;
import com.elabelz.util.TestUtil;

public class CheckoutPageTest extends TestBase{
	
	
		
		HomePage homePage;
		TestUtil testUtil;
		LoginPage loginPage;
		CheckoutPage checkoutPage;
		
		public CheckoutPageTest(){
			super();
		}
		
		@BeforeMethod
		public void setUp(){
			initialization();
			testUtil = new TestUtil();
			homePage = new HomePage();
			loginPage = homePage.clickOnSignIn();
			checkoutPage = loginPage.login(prop.getProperty("username"), prop.getProperty("password"));
		}
		
		
		  @Test(priority=1) 
		  public void purchaseProductTest(){ 
			  
			  checkoutPage.purchase();
			  String s = checkoutPage.validatePurchasePage();
			  assertEquals("ORDER CONFIRMATION",s,"Page Not found");
		  
		  }
		  
		
		
		
		
		@AfterMethod
		public void tearDown(){
			driver.quit();
		}

}
