package com.elabelz.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.elabelz.base.TestBase;

public class LandingPage extends TestBase{
	
	
	@FindBy(className = "info-account")
	WebElement account;
	
	public LandingPage(){
		PageFactory.initElements(driver, this);
	}
	
	//Actions:
	public String validateLoginPageTitle(){
		
		
		return account.getText();
	}

	
}
