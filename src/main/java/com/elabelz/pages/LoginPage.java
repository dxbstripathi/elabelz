package com.elabelz.pages;



import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.elabelz.base.TestBase;



public class LoginPage extends TestBase {
	
	

	
	//Page Factory - OR:
		@FindBy(id="email")
		WebElement username;
		
		@FindBy(id="passwd")
		WebElement password;
		
		@FindBy(id="SubmitLogin")
		WebElement loginBtn;
		
		@FindBy(className = "info-account")
		WebElement account;
		
		
		
		//Initializing the Page Objects:
		public LoginPage(){
			PageFactory.initElements(driver, this);
		}
		
		//Actions:
		public String validateLoginPageTitle(){
			
			
			return account.getText();
		}
		
		
		
		public CheckoutPage login(String un, String pwd){
			username.sendKeys(un);
			password.sendKeys(pwd);
			//loginBtn.click();
			    	JavascriptExecutor js = (JavascriptExecutor)driver;
			    	js.executeScript("arguments[0].click();", loginBtn);
			    	
			return new CheckoutPage();
		}
		
		
		

}
