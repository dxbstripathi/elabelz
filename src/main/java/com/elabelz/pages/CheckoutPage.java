package com.elabelz.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.elabelz.base.TestBase;



import java.util.List;



public class CheckoutPage extends TestBase {
	

	
		@FindBy(css = "[href*='order&step=1']")
		private WebElement proceedToStep2;

		@FindBy(name = "Submit" )
		private WebElement submit;

		@FindBy(xpath = "*[@id='layer_cart']//a[@class and @title='Proceed to checkout']")
		private WebElement proceedToCheckout;

		@FindBy(className = "checker")
		private WebElement termsOfServiceCheckobx;

		@FindBy(className = "bankwire")
		private WebElement payBankWire;

		@FindBy(css = "#cart_navigation > button")
		private WebElement completeOrder;

		@FindBy(css = "#center_column > div")
		private WebElement successMessage;

		@FindBy(id = "summary_products_quantity")
		private WebElement itemsInCart;

		@FindBy(id = "cart_summary")
		private WebElement cartTable;

		@FindBy(css = "[class*='alert-warning']")
		private WebElement emptyCart;

		@FindBy(className = "sf-with-ul")
		private WebElement womenLabel;

		@FindBy(xpath = "//a[@title='Faded Short Sleeve T-shirts']/ancestor::li")
		WebElement shirt;
		
		@FindBy(className = "info-account")
		WebElement account;
		
		@FindBy(xpath="//*[@id='layer_cart']//a[@class and @title='Proceed to checkout']")
		WebElement proceedtoCheckout ;
		
		@FindBy(xpath="//*[contains(@class,'cart_navigation')]/a[@title='Proceed to checkout']")
		WebElement proceedtoCheckout2;
		
		@FindBy(name ="processAddress")
		WebElement processAddress;
		
		@FindBy(id="uniform-cgv")
		WebElement uniform;
		
		@FindBy(name="processCarrier")
		WebElement carrier;
		
		@FindBy(className = "bankwire")
		WebElement backWire;
		
		@FindBy(xpath="//*[@id='cart_navigation']/button")
		WebElement cartButton;
		
		@FindBy(css="h1")
		WebElement heading1;
		
		WebDriverWait wait = new WebDriverWait(driver, 90);
		
	

		public CheckoutPage(){
			PageFactory.initElements(driver, this);
		}
		
		public  void  purchase() {
			womenLabel.click();
			shirt.click();
			shirt.click();
			
			WebElement e5 = wait.until(ExpectedConditions.visibilityOf(submit));
			e5.click();
			
			
			WebElement e1= wait.until(ExpectedConditions.visibilityOf(proceedtoCheckout));
			e1.click();
			WebElement e2 = wait.until(ExpectedConditions.visibilityOf(proceedtoCheckout2));
			e2.click();
			WebElement e3 = wait.until(ExpectedConditions.visibilityOf(processAddress));
			e3.click();
			WebElement e4 = wait.until(ExpectedConditions.visibilityOf(uniform));
			e4.click();
			carrier.click();
			backWire.click();
			cartButton.click();
			
		
		
		}

		public String validatePurchasePage() {
			
			WebElement heading = wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("h1")));
			return heading.getText();
			
		}

		public String validateLoginPageTitle() {
			return account.getText();
			
		}
		
		
	

	

}
