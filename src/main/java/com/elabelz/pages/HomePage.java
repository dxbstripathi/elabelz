package com.elabelz.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.elabelz.base.TestBase;

public class HomePage extends TestBase{
	
	//Page Factory - OR:
	@FindBy(className="login")
	WebElement signButton;
	

	
	//Initializing the Page Objects:
	public HomePage(){
		PageFactory.initElements(driver, this);
	}
	
	
	public String verifyHomePageTitle(){
		return driver.getTitle();
	}



public LoginPage  clickOnSignIn(){
		
		
		WebDriverWait wait = new WebDriverWait(driver, 90);
		WebElement element = wait.until(ExpectedConditions.elementToBeClickable(signButton));
		this.highLightElement(driver, element);
		element.click();
	
		return new LoginPage();
		
		
	}

}
